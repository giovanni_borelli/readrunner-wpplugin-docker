#! /bin/bash

if [ "$1" = "-k" ]
then
	echo "Killing containers"
	docker rm -f  $(docker ps -a -q)
else
	echo "Gracefully stopping containers"
	docker stop $(docker ps -a -q)
	docker rm -f  $(docker ps -a -q)
fi
